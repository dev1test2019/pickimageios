import UIKit
import Flutter
import DKImagePickerController
import Photos
import PhotoEditorSDK
import Foundation

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate, UINavigationControllerDelegate {
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        GeneratedPluginRegistrant.register(with: self)

        let controller: FlutterViewController = window?.rootViewController as! FlutterViewController
        let imagePickerChannel = ImagePickerChannel()
        imagePickerChannel.initPickImagerMethod(controller)
        
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
}
