//
//  UIImage.swift
//  Runner
//
//  Created by MACBOOK on 9/13/22.
//
import Foundation
import Accelerate
import Photos
import DKImagePickerController

struct ImageSizeInfo {
     var size: CGFloat
     var data: Data
     
     init(size: CGFloat, data: Data){
          self.size = size
          self.data = data
     }
}

extension UIImage{
    func getImageSizeInfo(scale: CGFloat, allowedUnits: ByteCountFormatter.Units) -> ImageSizeInfo? {
         let data = self.jpegData(compressionQuality: scale)!
         let formatter = ByteCountFormatter()
         formatter.allowedUnits = allowedUnits
         formatter.countStyle = ByteCountFormatter.CountStyle.memory
         let imageSize = formatter.string(fromByteCount: Int64(data.count))
         
         switch allowedUnits {
         case .useBytes:
              print("ImageSize(Bytes): \(imageSize)")
              return ImageSizeInfo(size: CGFloat(data.count), data: data)
         case .useKB:
              print("ImageSize(KB): \(imageSize)")
              return ImageSizeInfo(size: CGFloat(data.count) / 1024, data: data)
         case .useMB:
              print("ImageSize(MB): \(imageSize)")
              return ImageSizeInfo(size: CGFloat(data.count) / 1024 / 1024, data: data)
         case .useGB:
              print("ImageSize(GB): \(imageSize)")
              return ImageSizeInfo(size: CGFloat(data.count)  / 1024 / 1024 / 1024, data: data)
         default:
              print("ImageSize(KB): \(imageSize)")
              return ImageSizeInfo(size: CGFloat(data.count) / 1024, data: data)
         }
    }
}
