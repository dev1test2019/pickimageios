//
//  ImagePickerChannel.swift
//  Runner
//
//  Created by MACBOOK on 9/15/22.
//
import UIKit
import Flutter
import DKImagePickerController
import Photos
import Foundation

class ImagePickerChannel: FlutterAppDelegate, UINavigationControllerDelegate {
    func initPickImagerMethod(_ controller: FlutterViewController){
        //MARK: Choose nhiều image
        let methodChanelName = "pickImage"
        let pickImageChanel = FlutterMethodChannel(
            name: methodChanelName,
            binaryMessenger: controller.binaryMessenger,
            codec: FlutterStandardMethodCodec.sharedInstance()
        )
        pickImageChanel.setMethodCallHandler {( call: FlutterMethodCall , result: @escaping FlutterResult) -> Void in
            switch call.method {
            case "pickImage":
                self.openLibraryOption(controller, call, result, false)
            default:
                result(FlutterMethodNotImplemented)
            }
        }
        
        //MARK: Choose 1 image
        let pickOneMethodChanelName = "pickOneImage"
        let pickOneImageChanel = FlutterMethodChannel(
            name: pickOneMethodChanelName,
            binaryMessenger: controller.binaryMessenger,
            codec: FlutterStandardMethodCodec.sharedInstance()
        )
        pickOneImageChanel.setMethodCallHandler {( call: FlutterMethodCall , result: @escaping FlutterResult) -> Void in
            switch call.method {
            case "pickOneImage":
                self.openLibraryOption(controller, call, result, true)
            default:
                result(FlutterMethodNotImplemented)
            }
        }
    }
    func openLibraryOption(_ controller: FlutterViewController, _ call: FlutterMethodCall,_ result: @escaping FlutterResult,_ isPickOneImage:Bool?){
        let pickerController = DKImagePickerController()
        
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedString.Key.foregroundColor : UIColor.black
        ]
        if #available(iOS 13.0, *) {
            UINavigationBar.appearance().backgroundColor = UIColor.white
        } else {
        }
        UINavigationBar.appearance().tintColor = UIColor.black

        pickerController.sourceType = .photo
        pickerController.assetType = .allAssets
        pickerController.showsCancelButton = true

        pickerController.delegate = self
        pickerController.allowSwipeToSelect = true
        
        if isPickOneImage == false {
            pickerController.singleSelect = false
        } else {
            pickerController.singleSelect = true
        }
        
        var photoPaths:[String?] = []
        var photoPath:String = ""
        
        pickerController.didSelectAssets = {[weak self] (assets: [DKAsset]) in
            for asset in assets {
                let option = PHImageRequestOptions()
                asset.fetchOriginalImage(options: option, completeBlock: { image, info in
                    if let image = image {
                        if isPickOneImage == true {
                            photoPath = (self?.saveToFile(image: image))!
                            result(photoPath)
                        } else {
                            photoPaths.append(self?.saveToFile(image: image))
                            if photoPaths.count == assets.count {
                                result(photoPaths)
                            }
                        }
                    }
                })
            }
        }
        controller.present(pickerController, animated: true, completion: nil)
    }

    private func saveToFile(image: UIImage) -> String {
        let data = image.jpegData(compressionQuality: 1.0)
        let tempDir = NSTemporaryDirectory()
        let imageName = "image_picker_\(ProcessInfo().globallyUniqueString).jpg"
        let filePath = tempDir.appending(imageName)
        if FileManager.default.createFile(atPath: filePath, contents: data, attributes: nil) {
            return filePath
        }
        return ""
    }
}
