import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';

enum ImageSource { photos, camera }

String _stringImageSource(ImageSource imageSource) {
  switch (imageSource) {
    case ImageSource.photos:
      return 'photos';
    case ImageSource.camera:
      return 'camera';
  }
}

abstract class ImagePicker {
  Future<List<File?>> choosePhoto({ImageSource imageSource});
  Future<File?> chooseOnePhoto({ImageSource? imageSource});
}

class ImagePickerChannel implements ImagePicker {
  static const platform = MethodChannel('dk_image_picker');
  static const platformPickOne = MethodChannel('dk_image_picker');
  @override
  Future<List<File?>> choosePhoto({ImageSource? imageSource}) async {
    var stringImageSource = _stringImageSource(imageSource!);
    var results = await platform.invokeMethod('pickImage', stringImageSource);
    List<File?> photos = [];
    for (var path in results) {
      photos.add(File(path));
    }
    return photos;
  }

  @override
  Future<File?> chooseOnePhoto({ImageSource? imageSource}) async {
    var stringImageSource = _stringImageSource(imageSource!);
    var result = await platformPickOne.invokeMethod('pickOneImage', stringImageSource);
    if (result is String) {
      return File(result);
    }
    return null;
  }
}
