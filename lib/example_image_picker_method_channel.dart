import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'example_image_picker_platform_interface.dart';

/// An implementation of [ExampleImagePickerPlatform] that uses method channels.
class MethodChannelExampleImagePicker extends ExampleImagePickerPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('example_image_picker');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }
}
