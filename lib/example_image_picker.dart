import 'example_image_picker_platform_interface.dart';

export 'src/src.dart';

class ExampleImagePicker {
  Future<String?> getPlatformVersion() {
    return ExampleImagePickerPlatform.instance.getPlatformVersion();
  }
}
